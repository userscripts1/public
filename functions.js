function isCurrentUrl(url) {
  return window.location.href.indexOf(url) === 0
}

function Button(value, top, left, click, can) {
  const input = document.createElement('input')
  const border = '2px solid #111'
  if (can()) {
    input.style.cursor = 'pointer'
    input.onmouseover = () => {
      input.style.backgroundColor = '#111'
      input.style.color = '#f1f1f1'
      input.style.border = border
    }

    input.onmouseout = () => {
      input.style.backgroundColor = '#111'
      input.style.color = '#818181'
      input.style.border = border
    }
    input.onclick = click
  } else {
    input.style.opacity = '0.6'
    input.style.cursor = 'not-allowed'
  }
  input.style.fontSize = '22px'
  input.style.position = 'fixed'
  input.style.top = top
  input.style.left = left
  input.style.backgroundColor = '#111'
  input.style.border = border
  input.style.color = '#818181'
  input.style.textAlign = 'center'
  input.style.zIndex = '10'
  input.type = 'button'
  input.value = value
  input.style.transitionDuration = '0.4s'
  document.body.appendChild(input)
  return input
}

function addGlobalStyle(css) {
  const [head] = document.getElementsByTagName('head')
  if (!head) {
    return
  }
  const style = document.createElement('style')
  style.type = 'text/css'
  style.innerHTML = css
  head.appendChild(style)
}

function toStyleStr(obj, selector) {
  const stack = []
  let key = null
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      stack.push(`${key}:${obj[key]}`)
    }
  }

  if (selector) {
    return `${selector}{${stack.join(';')}}`
  } else {
    return stack.join(';')
  }
}


let counter = 0
function debug(...message) {
  counter += 1
  const css = 'background: white; color: green; display: block;'
  console.log(`%cDebug: ${counter}:`, css, ...message)
}
