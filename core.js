function isSavedInFavorites(showName) {
  const seriesInNav = document
    .getElementById('other-series-nav')
    .getElementsByTagName('li')
  for (const series of seriesInNav) {
    if (series.getElementsByTagName('a')[0].innerHTML === showName) {
      return true
    }
  }
  return false
}

function showNotLoggedInBox() {
  addGlobalStyle(toStyleStr({
    padding: '10px',
    'background-color': '#f44336',
    color: 'white',
    top: '0',
    position: 'fixed',
    'z-index': '10'
  }, '.alert'))

  const alertMessage = document.createElement('div')
  alertMessage.setAttribute('class', 'alert')
  alertMessage.innerHTML = 'Für das Skript musst du eingeloggt sein.'
  document.body.appendChild(alertMessage)
}
